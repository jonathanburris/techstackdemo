﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
namespace ServiceManager.Tests
{
    [TestClass()]
    public class UserServiceManagerTests
    {
        [TestMethod()]
        public void GetUsersTest()
        {
            UserServiceManager mgr = new UserServiceManager();
            Assert.IsTrue(mgr.GetUsers().Count > 0);
        }

        [TestMethod()]
        public void GetUserTest()
        {
            UserServiceManager mgr = new UserServiceManager();

            string userName = "janedoe";
            Assert.IsNotNull(mgr.GetUser(userName));
        }

        [TestMethod()]
        public void CreateTest()
        {
            UserServiceManager mgr = new UserServiceManager();
            User user = new User();
            user.UserName = "testuser";
            user.FirstName = "Test";
            user.LastName = "User";

            mgr.Create(user);

            Assert.IsTrue(mgr.GetUsers().Contains(user));
            
        }

        [TestMethod()]
        public void UpdateTest()
        {
            UserServiceManager mgr = new UserServiceManager();

            Assert.Inconclusive();
        }

        [TestMethod()]
        public void DeleteTest()
        {
            UserServiceManager mgr = new UserServiceManager();

            User user = new User();
            user.UserName = "deleteuser";
            user.FirstName = "Delete";
            user.LastName = "User";

            mgr.Create(user);
            mgr.Delete(user.UserName);

            Assert.IsFalse(mgr.GetUsers().Contains(user));
        }
    }
}
