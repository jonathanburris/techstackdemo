﻿users.config(['$routeProvider', function ($routeProvider) {
    // Our routes go in here
    $routeProvider.
        when('/', {
            templateUrl: 'partials/users/home.html'
        }).
        when('/users', {
            templateUrl: 'partials/users/users.html',
            controller: 'UserCtrl'
        }).
        when('/about', {
            templateUrl: 'partials/users/about.html'
        }).
        otherwise({ redirectTo: '/' });
}]).config(['$translateProvider', function ($translateProvider) {


    $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
    });

    $translateProvider.fallbackLanguage('en')
        .registerAvailableLanguageKeys(['en', 'fr'], {
            'en_US': 'en',
            'en_UK': 'en',
            'en_CA': 'en',
            'fr_FR': 'fr',
            'fr_CA': 'fr'
        })
        .determinePreferredLanguage();
        //.useLocalStorage();

    //$translateProvider.preferredLanguage('fr');


}]);