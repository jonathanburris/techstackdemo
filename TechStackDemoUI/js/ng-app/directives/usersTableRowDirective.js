﻿users.directive('usersTableRow', function () {

    return {

        restrict: 'EAC',
        replace: true,
        transclude: true,
        template: '<tr ng-repeat="user in users | filter:query track by user.UserName"><td><button type="button" class="btn btn-default btn-xs" ng-click="editUser(user)" title="Edit" data-toggle="modal" data-target="#editUserModal"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger btn-xs" ng-click="deleteUser(user)" title="Delete"><i class="fa fa-trash-o"></i></button></td><td>{{user.UserName}}</td><td>{{user.FirstName}}</td><td>{{user.LastName}}</td></tr>',
        link: function (scope, element, attrs) {

        }

    };

});