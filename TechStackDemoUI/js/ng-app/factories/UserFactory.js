﻿var UserFactory = global.factory('UserFactory', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {

    return {

        getUsers: function () {

            return $http.get($rootScope.apiUrl + 'users');

        },

        createUser: function (user) {

            return $http.post($rootScope.apiUrl + 'users', user);

        },

        updateUser: function (user) {

            return $http.put($rootScope.apiUrl + 'users/' + user.UserName, user);

        },

        deleteUser: function (user) {

            return $http.delete($rootScope.apiUrl + 'users/' + user.UserName);

        }

    }

}]);