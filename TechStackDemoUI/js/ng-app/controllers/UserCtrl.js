﻿var UserCtrl = users.controller('UserCtrl', ['$rootScope', '$scope', '$translate', 'UserFactory', function ($rootScope, $scope, $translate, UserFactory) {

    $scope.userFactory = UserFactory;

    $scope.init = function () {
        ///TODO: any controller initialization (i.e. check permissions)
        $scope.getUsers();

        $translate('CONFIRM_DELETE_USER').then(function (text) { $scope.confirmDeleteUserCaption = text; });
    }

    $scope.getUsers = function () {
        $scope.userFactory.getUsers().success(function (users) {
            $scope.users = users;
        }).error(function (error) {
            $scope.status = 'Unable to load users: ' + error.message;
        });
    };

    $scope.createUser = function () {
        $scope.userFactory.createUser($scope.user).success(function () {
            $scope.getUsers();
            $scope.user = { UserName: '', FirstName: '', LastName: '' };
        }).error(function (error) {
            $scope.status = 'Unable to create user: ' + error.message;
        });
    };

    $scope.updateUser = function () {
        $scope.userFactory.updateUser($scope.userToEdit).success(function () {
            $('#editUserModal').modal('hide');
            $scope.getUsers();
            $scope.userToEdit = { UserName: '', FirstName: '', LastName: '' };
        }).error(function (error) {
            $scope.status = 'Unable to create user: ' + error.message;
        });
    };

    $scope.deleteUser = function (user) {
        if (!confirm($scope.confirmDeleteUserCaption))
            return;

        $scope.userFactory.deleteUser(user).success(function () {
            $scope.getUsers();
        }).error(function (error) {
            $scope.status = 'Unable to delete users: ' + error.message;
        });
    };

    $scope.editUser = function (user) {
        $scope.userToEdit = user;
    };

}]);