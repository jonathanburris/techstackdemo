﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceManager
{
    public interface IUserServiceManager
    {
        List<User> GetUsers();
        User GetUser(string userName);
        void Create(User user);
        void Update(string userName, User user);
        void Delete(string userName);
    }
}
