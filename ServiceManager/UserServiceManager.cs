﻿using DataProvider;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceManager
{
    public class UserServiceManager : IUserServiceManager
    {
         IUserDataProvider _provider = null;

        public UserServiceManager()
        {
            _provider = new FakeUserDataProvider();
        }

        public UserServiceManager(IUserDataProvider provider)
        {
            _provider = provider;
        }

        public List<User> GetUsers()
        {
            return _provider.GetUsers();
        }

        public User GetUser(string userName)
        {
            return _provider.GetUser(userName);
        }

        public void Create(User user)
        {
            try
            {
                validate(user);
                _provider.Create(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string userName, User user)
        {
            try
            {
                validate(user);
                _provider.Update(userName, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string userName)
        {
            try
            {
                _provider.Delete(userName);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private void validate(User user)
        {

            if (user.UserName.Length == 0 || user.FirstName.Length == 0 || user.LastName.Length == 0)
                throw new Exception("Validation failed");

        }
    }
}
