﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataProvider
{
    public class FakeUserDataProvider : IUserDataProvider
    {
        public static List<User> _users;

        public FakeUserDataProvider()
        {
            if (_users == null)
            {
                _users = new List<User>();

                User user;

                user = new User();
                user.UserName = "johndoe";
                user.FirstName = "John";
                user.LastName = "Doe";
                _users.Add(user);

                user = new User();
                user.UserName = "janedoe";
                user.FirstName = "Jane";
                user.LastName = "Doe";
                _users.Add(user);
            }

        }

        public List<User> GetUsers()
        {
            return _users;
        }

        public User GetUser(string userName)
        {
            return _users.FirstOrDefault(u => u.UserName == userName);
        }

        public void Create(User user)
        {
            _users.Add(user);
        }

        public void Update(string userName, User user)
        {
            User userToEdit = _users.FirstOrDefault(u => u.UserName == userName);

            _users.Remove(userToEdit);
            _users.Add(user);
        }

        public void Delete(string userName)
        {
            User userToRemove = _users.FirstOrDefault(u => u.UserName == userName);
            _users.Remove(userToRemove);
        }

    }
}
