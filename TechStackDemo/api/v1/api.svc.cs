﻿using Model;
using ServiceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace TechStackDemo.api.v1
{

    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class api
    {
        [OperationContract]
        [WebGet(UriTemplate = "users")]
        public List<User> GetUsers()
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/json";

            IUserServiceManager userServiceManager = new UserServiceManager();

            return userServiceManager.GetUsers();
        }

        [OperationContract]
        [WebGet(UriTemplate = "users/{userName}")]
        public User GetUser(string userName)
        {
            IUserServiceManager userServiceManager = new UserServiceManager();

            return userServiceManager.GetUser(userName);

        }

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "users")]
        public void CreateUser(User user)
        {
            IUserServiceManager userServiceManager = new UserServiceManager();
            userServiceManager.Create(user);
        }

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "users/{userName}")]
        public void UpdateUser(string userName, User user)
        {
            IUserServiceManager userServiceManager = new UserServiceManager();
            userServiceManager.Update(userName, user);
        }

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "users/{userName}")]
        public void DeleteUser(string userName)
        {
            IUserServiceManager userServiceManager = new UserServiceManager();
            userServiceManager.Delete(userName);
        }

    }
}
